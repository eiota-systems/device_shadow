defmodule DeviceShadow.MixProject do
  use Mix.Project

  def project do
    [
      app: :device_shadow,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      releases: releases(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {DeviceShadow, []}
    ]
  end

  defp releases do
    [
      app: [
        include_executables_for: [:unix],
        applications: [device_shadow: :permanent, runtime_tools: :permanent],
        cookie: "device_shadow_cookie"
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:bbmustache, "~> 1.9"},
      {:cluster_kv,
       git: "https://gitlab.com/entropealabs/cluster_kv",
       tag: "e1dfa31b10afba62d513688f21da64c3629ea65c"},
      {:elixpath, "~> 0.1.0"},
      {:wampex_client,
       git: "https://gitlab.com/entropealabs/wampex_client.git",
       tag: "93704158ba2fb4db7bb19a454dad3367ecf5d03f"}
    ]
  end
end
