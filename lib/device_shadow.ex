defmodule DeviceShadow do
  @moduledoc """
  Documentation for DeviceShadow.
  """
  alias DeviceShadow.{RealmSupervisor, RealmSync}
  alias Wampex.Client
  alias Wampex.Client.Session
  alias Wampex.Client.{Authentication, Realm}
  alias Wampex.Roles.{Caller, Subscriber}

  def start(_, _) do
    url = System.get_env("WAMP_SERVER") || "ws://localhost:4000/ws"
    platform_password = System.get_env("PLATFORM_PASSWORD")
    admin_realm = System.get_env("ADMIN_REALM")
    quorum = (System.get_env("QUORUM") || "1") |> String.to_integer()
    replicas = (System.get_env("REPLICAS") || "1") |> String.to_integer()
    topologies = Application.get_env(:device_shadow, :topologies)

    authentication = %Authentication{
      authid: System.get_env("ADMIN_AUTHID"),
      authmethods: ["wampcra"],
      secret: System.get_env("ADMIN_PASSWORD")
    }

    realm = %Realm{name: admin_realm, authentication: authentication}

    admin_roles = [Caller, Subscriber]
    admin_session = %Session{url: url, realm: realm, roles: admin_roles}

    children = [
      {ClusterKV,
       name: DeviceShadowKV, topologies: topologies, replicas: replicas, quorum: quorum},
      {RealmSupervisor, platform_password: platform_password, url: url},
      {Client, name: AdminClient, session: admin_session, reconnect: true},
      {RealmSync, client: AdminClient, admin_realm: admin_realm}
    ]

    opts = [strategy: :one_for_one, name: DeviceShadow.Supervisor, max_restarts: 10]
    Supervisor.start_link(children, opts)
  end
end
