defmodule DeviceShadow.RealmWorker do
  use GenServer
  require Logger

  alias DeviceShadow.RealmWorkerAgent
  alias Wampex.Client
  alias Wampex.Roles.Broker.Event
  alias Wampex.Roles.Callee
  alias Wampex.Roles.Callee.{Register, Unregister, Yield}
  alias Wampex.Roles.Caller.Call
  alias Wampex.Roles.Dealer.Invocation
  alias Wampex.Roles.Publisher.Publish
  alias Wampex.Roles.Subscriber.Subscribe

  @connect "device.connect"
  @set_attributes "device.set_attributes"
  @get_attributes "device.get_attributes"
  @get_profile "device.get_profile"
  @get_recent_topic_data "device.get_recent_topic_data"
  @get_by_type "device.get_by_type"
  @get_all "device.get_all"

  def start_link(tenant: tenant, realm: realm, client: client, tasks: tasks, agent: agent) do
    GenServer.start_link(__MODULE__, {tenant, realm, client, tasks, agent},
      name: worker_name(realm)
    )
  end

  @impl true
  def init({tenant, realm, client, tasks, agent}) do
    Client.add(client, self())
    id = register(client)
    profiles = RealmWorkerAgent.get_profiles(agent)

    subs =
      Enum.flat_map(profiles, fn {id, profile} ->
        subscribe(client, id, profile, realm)
      end)

    subscribe_disconnect(client)

    {:ok,
     %{
       tenant: tenant,
       realm: realm,
       client: client,
       agent: agent,
       tasks: tasks,
       subscriptions: subs,
       registration_id: id
     }}
  end

  @impl true
  def handle_info({:connected, _client}, state) do
    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Invocation{
          request_id: rid,
          details: %{"procedure" => @connect, "session_id" => session_id},
          arg_kw: %{"id" => id, "profile" => profile}
        },
        %{realm: realm, client: client, agent: agent, subscriptions: subscriptions} = state
      ) do
    now = DateTime.utc_now() |> DateTime.to_iso8601()
    ClusterKV.update(DeviceShadowKV, realm, "#{id}.profile", profile, &update_profile/2)
    ClusterKV.update(DeviceShadowKV, realm, "#{session_id}.id", id, &update_profile/2)
    device_type = get_in(profile, ["system", "type"])
    ClusterKV.update(DeviceShadowKV, realm, "#{device_type}.devices", id, &update_devices/2)
    ClusterKV.update(DeviceShadowKV, realm, "devices", id, &update_devices/2)

    ClusterKV.update(
      DeviceShadowKV,
      realm,
      "#{id}.attributes",
      {:connected_at, now},
      &update_attribute/2
    )

    {_, attributes} = ClusterKV.get(DeviceShadowKV, realm, "#{id}.attributes")

    Client.yield(client, %Yield{
      request_id: rid,
      arg_list: Enum.map(attributes, &Tuple.to_list/1),
      arg_kw: %{success: true}
    })

    RealmWorkerAgent.add_profile(agent, {id, profile})
    subs = subscribe(client, id, profile, realm)
    {:noreply, %{state | subscriptions: subs ++ subscriptions}}
  end

  @impl true
  def handle_info(
        %Invocation{
          request_id: rid,
          details: %{"procedure" => @get_all}
        },
        %{realm: realm, client: client} = state
      ) do
    device_ids =
      case ClusterKV.get(DeviceShadowKV, realm, "devices") do
        :not_found -> []
        {_, ids} -> ids
      end

    devices = get_devices(device_ids, realm)

    Client.yield(client, %Yield{
      request_id: rid,
      arg_list: devices
    })

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Invocation{
          request_id: rid,
          details: %{"procedure" => @get_by_type},
          arg_kw: %{"type" => type}
        },
        %{realm: realm, client: client} = state
      ) do
    device_ids =
      case ClusterKV.get(DeviceShadowKV, realm, "#{type}.devices") do
        :not_found -> []
        {_, ids} -> ids
      end

    devices = get_devices(device_ids, realm)

    Client.yield(client, %Yield{
      request_id: rid,
      arg_list: devices
    })

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Invocation{
          request_id: rid,
          details: %{"procedure" => @set_attributes},
          arg_list: attributes,
          arg_kw: %{"id" => id}
        },
        %{realm: realm, client: client} = state
      ) do
    Enum.each(attributes, fn [key, value] ->
      ClusterKV.update(
        DeviceShadowKV,
        realm,
        "#{id}.attributes",
        {String.to_atom(key), value},
        &update_attribute/2
      )
    end)

    Client.yield(client, %Yield{
      request_id: rid,
      arg_kw: %{success: true}
    })

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Invocation{
          request_id: rid,
          details: %{"procedure" => @get_attributes},
          arg_kw: %{"id" => id}
        },
        %{realm: realm, client: client} = state
      ) do
    res =
      case ClusterKV.get(
             DeviceShadowKV,
             realm,
             "#{id}.attributes"
           ) do
        {_k, attributes} -> attributes
        _ -> []
      end

    Client.yield(client, %Yield{
      request_id: rid,
      arg_list: Enum.map(res, &Tuple.to_list/1)
    })

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Invocation{
          request_id: rid,
          details: %{"procedure" => @get_profile},
          arg_kw: %{"id" => id}
        },
        %{realm: realm, client: client} = state
      ) do
    res =
      case ClusterKV.get(
             DeviceShadowKV,
             realm,
             "#{id}.profile"
           ) do
        {_k, [profile]} -> profile
        _ -> {}
      end

    Client.yield(client, %Yield{
      request_id: rid,
      arg_kw: res
    })

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Invocation{
          request_id: rid,
          details: %{"procedure" => @get_recent_topic_data},
          arg_kw: %{"id" => id, "topic" => topic}
        },
        %{realm: realm, client: client} = state
      ) do
    data =
      case ClusterKV.get(
             DeviceShadowKV,
             realm,
             "#{id}.topic.#{topic}"
           ) do
        :not_found -> []
        {_, data} -> data
      end

    Client.yield(client, %Yield{
      request_id: rid,
      arg_list: Enum.map(data, &Tuple.to_list/1)
    })

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Event{
          details: %{"topic" => "router.peer.disconnect", "session_id" => session_id}
        },
        %{realm: realm, tasks: tasks} = state
      ) do
    Task.Supervisor.start_child(tasks, fn ->
      case ClusterKV.get(DeviceShadowKV, realm, "#{session_id}.id") do
        {_, [id]} ->
          now = DateTime.utc_now() |> DateTime.to_iso8601()

          ClusterKV.update(
            DeviceShadowKV,
            realm,
            "#{id}.attributes",
            {:disconnected_at, now},
            &update_attribute/2
          )

        _ ->
          :noop
      end
    end)

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Event{publication_id: pid, arg_kw: data, details: %{"topic" => topic}},
        %{tenant: tenant, realm: realm, tasks: tasks, client: client} = state
      ) do
    Task.Supervisor.start_child(tasks, fn ->
      case ClusterKV.update_if(DeviceShadowKV, realm, topic, pid, &update_if/2) do
        :updated -> handle_event(client, tenant, realm, topic, data)
        :noop -> :noop
      end
    end)

    {:noreply, state}
  end

  @impl true
  def handle_info(event, state) do
    Logger.info("Realm #{state.realm} received event #{inspect(event)}")
    {:noreply, state}
  end

  @doc """
  Unregister ourselves and reassign the subscriptions to another DeviceShadow node
  """
  @impl true
  def terminate(_reason, %{
        registration_id: rid,
        subscriptions: subs,
        client: client,
        realm: realm
      }) do
    Client.cast(client, Callee.unregister(%Unregister{registration_id: rid}))

    Enum.each(subs, fn {id, _, _} ->
      {_, [profile]} = ClusterKV.get(DeviceShadowKV, realm, "#{id}.profile")

      Client.call(client, %Call{
        procedure: @connect,
        arg_kw: %{id: id, profile: profile}
      })
    end)
  end

  defp get_devices(device_ids, realm) do
    Enum.map(device_ids, fn id ->
      attrs =
        case ClusterKV.get(
               DeviceShadowKV,
               realm,
               "#{id}.attributes"
             ) do
          :not_found -> []
          {_, attrs} -> attrs
        end

      profile =
        case ClusterKV.get(
               DeviceShadowKV,
               realm,
               "#{id}.profile"
             ) do
          :not_found -> %{}
          {_, [profile]} -> profile
        end

      %{id: id, attributes: Enum.map(attrs, &Tuple.to_list/1), profile: profile}
    end)
  end

  defp handle_event(client, tenant, realm, topic, %{"id" => id, "data" => data}) do
    now = DateTime.utc_now() |> DateTime.to_iso8601()
    profile = get_profile(realm, id)
    {metrics, events} = parse_data(id, data, profile, topic)
    update_topics(realm, id, events, now)
    update_topics(realm, id, metrics, now)
    categories = get_in(profile, ["categories"])
    type = get_in(profile, ["system", "type"])

    kw = %{
      id: id,
      realm: realm,
      tenant: tenant,
      categories: categories,
      type: type
    }

    do_publish(client, "device.metrics.#{type}.#{id}", metrics, kw)
    do_publish(client, "device.events.#{type}.#{id}", events, kw)
  end

  def do_publish(client, topic, list, kw) do
    Client.publish(client, %Publish{topic: topic, arg_list: list, arg_kw: kw})
  end

  def do_call(client, procedure, list, kw, timeout) do
    Client.call(
      client,
      %Call{procedure: procedure, arg_list: list, arg_kw: kw},
      timeout
    )
  catch
    :exit, er -> Logger.error("#{inspect(er)}")
  end

  defp update_topics(realm, id, vals, now) do
    Enum.each(vals, fn [topic, value] ->
      ClusterKV.update(
        DeviceShadowKV,
        realm,
        "#{id}.topic.#{topic}",
        {value, now},
        &update_topic/2
      )
    end)

    ClusterKV.update(
      DeviceShadowKV,
      realm,
      "#{id}.attributes",
      {:updated_at, now},
      &update_attribute/2
    )
  end

  defp update_if({key, [old]}, val) do
    case old == val do
      true -> :noop
      false -> {:update, {key, [val]}}
    end
  end

  defp parse_data(id, data, %{"metric_parsers" => mp, "event_parsers" => ep} = profile, topic) do
    metrics =
      Enum.flat_map(mp, fn {template, fields} ->
        case render_topic(id, template, profile) do
          ^topic ->
            Enum.map(fields, fn p -> get_kv(p, data) end)

          _ ->
            []
        end
      end)
      |> map_topics(topic)

    events =
      Enum.flat_map(ep, fn {template, fields} ->
        case render_topic(id, template, profile) do
          ^topic ->
            Enum.map(fields, fn p -> get_kv(p, data) end)

          _ ->
            []
        end
      end)
      |> map_topics(topic)

    {metrics, events}
  end

  defp map_topics(vals, topic) do
    Enum.flat_map(vals, fn
      {k, v} when is_list(v) ->
        v
        |> Enum.with_index()
        |> Enum.map(fn {val, i} ->
          ["#{topic}.#{k}.#{i}", val]
        end)

      {k, v} ->
        [["#{topic}.#{k}", v]]
    end)
  end

  defp get_profile(realm, id) do
    {_key, [profile]} = ClusterKV.get(DeviceShadowKV, realm, "#{id}.profile")
    profile
  end

  defp get_kv(%{"key" => k, "path" => path, "list" => true}, data) do
    {:ok, v} = Elixpath.query(data, path)
    {k, v}
  end

  defp get_kv(%{"key" => k, "path" => path}, data) do
    {k, Elixpath.get!(data, path)}
  end

  defp update_attribute({k, old}, {key, _} = new), do: {k, List.keystore(old, key, 0, new)}
  defp update_profile({k, _old}, value), do: {k, [value]}
  defp update_topic({k, old}, value), do: {k, [value | old] |> Enum.take(30)}
  defp update_devices({k, old}, value), do: {k, [value | old] |> Enum.uniq()}

  defp register(client) do
    do_register(client, @get_attributes)
    do_register(client, @get_profile)
    do_register(client, @set_attributes)
    do_register(client, @get_recent_topic_data)
    do_register(client, @get_by_type)
    do_register(client, @get_all)
    do_register(client, @connect)
  end

  defp subscribe_disconnect(client) do
    Client.subscribe(client, %Subscribe{topic: "router.peer.disconnect"})
  end

  defp do_register(client, proc) do
    case Client.register(client, %Register{procedure: proc}) do
      {:ok, id} ->
        id

      _r ->
        :timer.sleep(100)
        do_register(client, proc)
    end
  end

  defp subscribe(client, id, %{"topics" => topics} = profile, realm) do
    Logger.info("Subscribing for #{id}")

    Enum.map(topics, fn %{"topic" => t} ->
      topic = render_topic(id, t, profile)
      {:ok, sub} = Client.subscribe(client, %Subscribe{topic: topic})
      Logger.info("Subscribing to #{topic}")
      ClusterKV.put(DeviceShadowKV, realm, "#{id}.topic.#{topic}", 0)
      {id, sub, topic}
    end)
  end

  defp render_topic(id, topic, profile) do
    :bbmustache.render(topic, Map.put(profile, "id", id), [{:key_type, :binary}])
  end

  defp worker_name(realm), do: Module.concat([__MODULE__, realm, Worker])
end
