# DeviceShadow

This application is responsible for registering edge devices and processing their device profiles.

When an edge device comes online, either for the first time, or after a network issue, reboot, etc it must send a connect message to the device shadow that includes it's device profile.

A device profile takes the form of

```json
{
  "version": "1.0.0",
  "namespace": "com.eiota",
  "displayName": "AgTech Gateway",
  "description": "Gateway for ponics monitoring",
  "categories": ["water quality", "air quality", "weather", "lights", "pumps", "energy"],
  "system": {
    "manufacturer": "EIOTA",
    "type": "agtech_gateway",
    "model": "beta"
  },
  "topics": [
    {
      "topic": "{{system.type}}.{{id}}",
      "payload": {}
    }
  ],
  "procedures": [
    {
      "procedure": "{{system.type}}.{{id}}.light",
      "arguments": {}
    },
    {
      "procedure": "{{system.type}}.{{id}}.pump",
      "arguments": {}
    },
    {
      "procedure": "{{system.type}}.{{id}}.image",
      "arguments": {}
    }
  ],
  "metric_parsers": {
    "{{system.type}}.{{id}}": [
      {"key": "energy.kw", "path": "$.energy.kw"},
      {"key": "energy.produced", "path": "$.energy.kw_received"},
      {"key": "energy.consumed", "path": "$.energy.kw_delivered"},
      {"key": "water_quality.do", "path": "$.water_quality.do"},
      {"key": "water_quality.conductivity", "path": "$.water_quality.ec[0]"},
      {"key": "water_quality.tds", "path": "$.water_quality.ec[1]"},
      {"key": "water_quality.salinity", "path": "$.water_quality.ec[2]"},
      {"key": "water_quality.sg", "path": "$.water_quality.ec[3]"},
      {"key": "water_quality.ph", "path": "$.water_quality.ph"},
      {"key": "water_quality.temperature", "path": "$.water_quality.temp"},
      {"key": "weather.humidity", "path": "$.weather.humidity"},
      {"key": "weather.temperature", "path": "$.weather.outdoor_temperature"},
      {"key": "weather.pressure", "path": "$.weather.pressure"},
      {"key": "weather.precipitation", "path": "$.weather.rain"},
      {"key": "weather.solar.radiation", "path": "$.weather.solar.radiation"},
      {"key": "weather.solar.intensity", "path": "$.weather.solar.intensity"},
      {"key": "weather.uv", "path": "$.weather.uv"},
      {"key": "weather.wind.direction", "path": "$.weather.wind.direction"},
      {"key": "weather.wind.speed", "path": "$.weather.wind.speed"},
      {"key": "weather.wind.gust", "path": "$.weather.wind.gust"}
    ]
  },
  "event_parsers": {
    "{{system.type}}.{{id}}": [
      {"key": "light.status", "path": "$.light.status"},
      {"key": "light.color", "path": "$.light.color"},
      {"key": "pump.status", "path": "$.pump.status"}
    ]
  }
}
```

The complete `connect` payload takes the form of

```json
{
  "id": "the unique device id, usually a MAC or something similar",
  "profile": {...device_profile}
}
```

The id is merged into the device profile and provided as the context for the [Mustache](https://mustache.github.io/) templates embedded in the device profile.

This device profile allows the device shadow system to extract relevant events and metrics from events published by the device. It also tells the device shadow which topics to subscribe to and what functions the device itself exposes.

Event and metric parsing are done using JSONPath.

When an event is received, parsing is done and two new events are published into the system with `device.metrics` and `device.events` prepended to the topic it was received from. eg; an event that was received with the topic `agtech_gateway.57f0` would be parsed and two new events would be published `device.metrics.agtech_gateway.57f0` and `device.events.agtech_gateway.57f0` respectively.

Example payload:

```json
{
  "topic": "device.metrics.agtech_gateway.57f0",
  "kw": {
    "id": "device_id",
    "realm": "org.entropealabs.clubhouse",
    "categories": ["air_quality", "weather", ...],
    "type": "agtech_gateway"
  },
  "list": [
    ["air_quality.voc", 5],
    ["weather.temperature", 22],
    ...
  ]
}
```


Many downstream services benefit from utilizing the parsed data payloads rather than having to also utilize the device profile to properly parse the event directly from the device. For example [AlertManager](https://gitlab.com/eiota-systems/alert_manager) and [TelemetryCollector](https://gitlab.com/eiota-systems/telemetry_collector) both listen to the sub topics `device.metrics` and `device.events` to handle their respective business logic.

In addition to handling device data, the device shadow system also allows for assigning arbitrary key-value records to a device. This can be thought of as a tag system, some example use cases may be setting the devices location within a building, or setting a unique name for the UI to utilize.
